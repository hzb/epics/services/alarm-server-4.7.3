FROM openjdk:14-jdk-buster

RUN apt-get update && apt-get install -y ca-certificates-java unzip wget git maven

WORKDIR /epics

# Phoebus alarm server 
RUN wget https://controlssoftware.sns.ornl.gov/css_phoebus/nightly/alarm-server.zip &&\
    unzip alarm-server.zip && rm -f alarm-server-4.7.3.zip && mv alarm-server-4.7.3 alarm-server && rm -f alarm-server.zip && cd alarm-server && mkdir config

VOLUME ["/epics/alarm-server/config"]

#Adding settings and configuration
#ENTRYPOINT ["java","-jar","/epics/alarm-server/service-alarm-server-4.7.3.jar"]

EXPOSE 25/tcp

#CMD [-list] 


